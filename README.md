minos-one
====

The minos-one live DVD is built on LFS see http://linuxfromscratch.org

helix is the release name of minos-one version 2.0 built on LFS 7.9 systemd

The directory lfs contains the linux from scratch base system:

* rootfs.iso - mount point / ext4 filesystem as ISO
* usr.iso - mount point /usr ext4 filesystem as ISO

The directory minimal contains a bootable ISO image formated with iso9660. The
live DVD contains a minimal Xorg set of applications including WindowMaker.

* minos-one-v2.0-minimal.iso

You might want it to write to a DVD with `cdrecord` likewise:

cdrecord -v minos-one-v2.0-minimal.iso

The full build is in the minos-one subdirectory and contains:

* minos-one-v2.0-h3lix.iso

The h3lix build contains a functional display manager lxdm. Note you have to set
password for user h3lix on TTY in order to be able to login. The password for
root and h3lix are empty. Mate is the only available desktop environment.

Minos one comes with many developer tools and full sources. The src.iso is located
in casper/ subdirectory of the ISO image and is not mounted per default. To do so
issue following:

mount -o loop /media/cdrom/casper/src.iso /usr/src/sources

The h3lix build contains the /opt directory containing Qt4 and Qt5. As well OpenJDK8,
ant and maven. Mercurial is included, too.

The system editor of choice is emacs, you may want to launch it within `mate-terminal`
by executing `emacs -nw`.

The h3lix build can have boot parameter livedev=/dev/sr0 or whatever fits your
optical drive containing the live system. h3lix is about 21G large so you probably
have to write it to a BlueRay disc.
